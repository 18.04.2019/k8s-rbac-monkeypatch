#!/bin/bash -x

for NS in $(kubectl get ns | grep -Ev 'kube|NAME' | awk '{print $1}'); do
  kubectl create clusterrolebinding ${NS}-service-account --clusterrole cluster-admin --serviceaccount=${NS}:${NS}-service-account || echo > /dev/null ;
done

