#!/bin/bash -e


cat <<EOF | kubectl apply -n kube-system -f - > /dev/null
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: gitlab
  namespace: kube-system
---
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: gitlab-cluster-admin
subjects:
- kind: ServiceAccount
  name: gitlab
  namespace: kube-system
roleRef:
  kind: ClusterRole
  name: cluster-admin
  apiGroup: rbac.authorization.k8s.io
EOF



ENDPOINT=$(kubectl cluster-info | grep 'Kubernetes master' | awk '/http/ {print $NF}' | sed -r "s/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[mGK]//g")
TOKEN=$(kubectl -n kube-system get secret $(kubectl get secrets -n kube-system | grep gitlab | awk '{print $1}') -o jsonpath="{['data']['token']}" | base64 --decode)
CA=$(kubectl -n kube-system get secret $(kubectl get secrets -n kube-system | grep gitlab | awk '{print $1}') -o jsonpath="{['data']['ca\.crt']}" | base64 --decode)
CABASE=$(kubectl -n kube-system get secret $(kubectl get secrets -n kube-system | grep gitlab | awk '{print $1}') -o jsonpath="{['data']['ca\.crt']}")
CONTEXT=$(kubectl config current-context)



echo "${ENDPOINT}"
echo "${TOKEN}"
echo "${CA}"



cat << EOF > config.yaml
apiVersion: v1
kind: Config
users:
- name: gitlab-cluster-admin
  user:
    token: ${TOKEN}
clusters:
- cluster:
    certificate-authority-data: ${CABASE}
    server: ${ENDPOINT}
  name: ${CONTEXT}
contexts:
- context:
    cluster: ${CONTEXT}
    user: gitlab-cluster-admin
  name: ${CONTEXT}
current-context: ${CONTEXT}
EOF


